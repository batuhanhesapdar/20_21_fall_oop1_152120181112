#include <iostream>
#include <cstdio>
using namespace std;

int main() {
    int intone;
    long longone;
    char charone;
    float floatone;
    double doubleone;
    
    scanf("%d %ld %c %f %lf", &intone, &longone, &charone, &floatone, &doubleone);
    printf("%d\n%ld\n%c\n%f\n%lf\n", intone, longone, charone, floatone, doubleone);
    
    return 0;
}
