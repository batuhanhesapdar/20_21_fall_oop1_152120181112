#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;

int main() {
    int number;
    cin >> number;
    int arr[number];
    for (int i = 0; i < number; i++) {
        cin >> arr[i];
    }
    for (int i = number-1; i >=0; i--) {
        cout << arr[i] << " ";
    }
    return 0;
}
