#include <stdio.h>
#include <stdlib.h>

void update(int *a,int *b) {
    int c = *a + *b;
    int d = abs(*a - *b);
    *a = c;
    *b = d;
}
int main() {
    int a, b;
    int *pointera = &a, *pointerb = &b;
    scanf("%d %d", &a, &b);
    update(pointera, pointerb);
    printf("%d\n%d", a, b);
    return 0;
}
