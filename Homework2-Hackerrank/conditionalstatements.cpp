#include <bits/stdc++.h>

using namespace std;

int main()
{
    int number;
    cin >> number;
    cin.ignore(numeric_limits<streamsize>::max(), '\n');
    string numbers[9] = {"one","two","three","four","five","six","seven","eight","nine"};
    
    if(number>0 && number<10)
    {
        cout<<numbers[number-1];
    }
    else 
    {
        cout<<"bigger than 9";
    }

    return 0;
}
