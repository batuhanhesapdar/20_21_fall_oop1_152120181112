#include <iostream>
#include <fstream>
#include <string>
#include <cstdio>
#include <sstream>
using namespace std;

int lineCount=0; // the variable that hold line index of input.txt to determine first line
int input=0; // the integer value where is on the first line of input.txt
string lineContent; //this string is holding the second line of the input.txt

void SumOfNumbers(int ourNumbers[]) // This function is calculate sum of the input array's elements. Then print it.
{
	int totalSum = 0;
	for(int i = 0; i < input; i++){
        totalSum+=ourNumbers[i];
    }
    cout<< "Sum is "<< totalSum<< "\n";
}
void ProductOfNumbers(int ourNumbers[])// This function is calculate product of the input array's elements. Then print it.
{
    int totalProduct = 1;
    for(int i = 0; i < input; i++){
        totalProduct*=ourNumbers[i];
    }
    cout<< "Product is "<< totalProduct<< "\n";
}
void AvarageOfNumbers(int ourNumbers[])// This function is calculate sum of the input array's elements. Then dividing it to number count to find avarage of elements of array.
{
    float avarage = 0;
    float totalSum = 0;
    for(int i = 0; i < input; i++){
        totalSum+=ourNumbers[i];
    }
    avarage = totalSum / input;
    cout<< "Avarage is "<< avarage<< "\n";
}
void SmallestOfNumbers(int ourNumbers[])// This function is find the smallest value of the array and print it.
{
    int smallest =ourNumbers[0];
    for(int i = 0; i < input; i++){
        if(ourNumbers[i]<smallest)
        {
        	smallest=ourNumbers[i];
		}
    }
    cout<< "Smallest is "<< smallest<< "\n";
}

void ReadNumberCount()// This function read the first line of input.txt. And convert the string value of first line to int. Assigning it to input variable.
{
   fstream newfile;
   newfile.open("input.txt",ios::in); 
   if (newfile.is_open())
    {  
	    while(lineCount==0 && getline(newfile, lineContent))
		{ 
		    lineCount++;
		}
	    newfile.close(); 
	}
	if(lineContent=="")
	{
		cout <<"There is no input. File is empty!"<< "\n";
	}  
	else
	{	
		if(sscanf(lineContent.c_str(), "%d", &input) != 1)
		{
			cout <<"Your input is incorrect format!"<< "\n";
		}
		
	}
}

int ReadNumbers(int LineLength) { //This function is read the second line of the input.txt. then split the string to int array. When we have array of int numbers,
	                              // Function call other functions with that array to start calcualting
	ifstream newfile("input.txt");
	string content;
	for (int i = 1; i <= LineLength; i++)
        getline(newfile, content);
	
    int numbers[input];
    int i = 0;
    stringstream ssin(content);
    while (ssin.good() && i < input){
        ssin >> numbers[i];
        ++i;
    }
    cout<<"Here is the numbers: ";
    for(int i = 0; i < input; i++){
        cout<<" "<<numbers[i];
    }
    cout<<" "<<endl;
    
    //Calling calc functions
    SumOfNumbers(numbers);
    ProductOfNumbers(numbers);
	AvarageOfNumbers(numbers);
	SmallestOfNumbers(numbers);
}


int main(){//Our main func that work at start.
	ReadNumberCount();
	ReadNumbers(input);
	return 0;
   
}


